package com.EB03.ProjectEB03;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import org.junit.Test;

import java.nio.charset.StandardCharsets;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class BufferUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void Test_001 () {
        Buffer buf = new Buffer(20);
        assertEquals(0,buf.getDataSize());
    }

    @Test
    public void Test_002 () throws Exception {
        Buffer buf = new Buffer(20);
        buf.write("Hello world!".getBytes(StandardCharsets.UTF_8));
        assertEquals(12, buf.getDataSize());
    }

    @Test
    public void Test_003 () throws Exception {
        Buffer buf = new Buffer(20);
        buf.write("01234567890123456789".getBytes(StandardCharsets.UTF_8));
        assertEquals(20, buf.getDataSize());
    }

    @Test
    public void Test_004 () throws Exception {
        Buffer buf = new Buffer(20);
        buf.write("012345678901234567890".getBytes(StandardCharsets.UTF_8));
        assertEquals(0, buf.getDataSize());
    }

    @Test
    public void Test_005 () throws Exception {
        Buffer buf = new Buffer(20);
        buf.write("Hello world!".getBytes(StandardCharsets.UTF_8));
        assertEquals("Hello world!",new String(buf.getAll()));
    }

    @Test
    public void Test_006 () {
        Buffer buf = new Buffer(20);
        assertThrows(Exception.class , () -> {buf.get();});
    }

    @Test
    public void Test_007 () throws Exception {
        Buffer buf = new Buffer(20);
        buf.write("Hello ".getBytes(StandardCharsets.UTF_8));
        buf.write("world!".getBytes(StandardCharsets.UTF_8));
        assertEquals("Hello world!",new String(buf.getAll()));
    }
}