package com.EB03.ProjectEB03;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class FrameProcessorUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void Test_001 () {
        FrameProcessor frameProcessor = new FrameProcessor();
        byte[] data = new byte[] {0x07,0x06};
        byte[] expect = new byte[] {0x05,0x00,0x02,0x07,0x06,0x0C, (byte) 0xF1,0x04};
        byte[] ret = frameProcessor.toFrame(data);
        assertEquals(Arrays.toString(expect),Arrays.toString(ret));
    }

    @Test
    public void Test_002 () {
        FrameProcessor frameProcessor = new FrameProcessor();
        byte[] expect = new byte[] {0x07,0x06,0x0C};

        frameProcessor.fromFrame((byte) 0x05);
        frameProcessor.fromFrame((byte) 0x00);
        frameProcessor.fromFrame((byte) 0x02);
        frameProcessor.fromFrame((byte) 0x07);
        frameProcessor.fromFrame((byte) 0x06);
        frameProcessor.fromFrame((byte) 0x0C);
        frameProcessor.fromFrame((byte) 0xF1);
        frameProcessor.fromFrame((byte) 0x04);

        byte[] ret = frameProcessor.getData().getFramePayload();

        assertEquals(Arrays.toString(expect),Arrays.toString(ret));
    }

    @Test
    public void Test_003 () {
        FrameProcessor frameProcessor = new FrameProcessor();

        frameProcessor.fromFrame((byte) 0x05);
        frameProcessor.fromFrame((byte) 0x00);
        frameProcessor.fromFrame((byte) 0x02);
        frameProcessor.fromFrame((byte) 0x07);
        frameProcessor.fromFrame((byte) 0x06);
        frameProcessor.fromFrame((byte) 0x0C);
        frameProcessor.fromFrame((byte) 0xF1);
        frameProcessor.fromFrame((byte) 0x04);

        int ret = frameProcessor.getData().getFrameLength();

        assertEquals(2,ret);
    }

    @Test
    public void Test_004 () {
        FrameProcessor frameProcessor = new FrameProcessor();

        frameProcessor.fromFrame((byte) 0x05);
        frameProcessor.fromFrame((byte) 0x00);
        frameProcessor.fromFrame((byte) 0x02);
        frameProcessor.fromFrame((byte) 0x07);
        frameProcessor.fromFrame((byte) 0x06);
        frameProcessor.fromFrame((byte) 0x0C);
        frameProcessor.fromFrame((byte) 0xF1);
        frameProcessor.fromFrame((byte) 0x04);

        boolean ret = frameProcessor.getData().getFrameValidity();

        assertEquals((boolean) true,ret);
    }

}