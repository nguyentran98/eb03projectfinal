package com.EB03.ProjectEB03;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

public class Slider extends View {

    // Valeur du customView
    private int mAngle=0;

    private Point mPrePoint = new Point();
    private int mAngleRotation = 0;
    private boolean mStartRotation =true;


    private Point mCenter = new Point();

    private boolean mDoubleClick = false;
    private boolean mDisableMove = false;

    // attribut d'activation
    private boolean mEnabled = true;


    // référence vers le listener
    private SliderChangeListener mSliderChangeListener = null;

    private final static float MIN_BORDER_DIAMETER = 100;


    // Dimensions par défaut
    private final static float DEFAULT_CENTER_DIAMETER = 80;
    private final static float DEFAULT_TRACK_DIAMETER = 120;
    private final static float DEFAULT_BORDER_DIAMETER = 130;
    private final static float DEFAULT_MINOR_STEP_WIDTH = 3;
    private final static float DEFAULT_MAJOR_STEP_WIDTH = 7;
    private final static float DEFAULT_MINOR_STEP_LENGTH = 15;
    private final static float DEFAULT_MAJOR_STEP_LENGTH = 25;


    //attributs de dimension (en pixels)
    private float mCenterDiameter;
    private float mTrackDiameter;
    private float mBorderDiameter;
    private float mMinorStepWidth;
    private float mMajorStepWidth;
    private float mMinorStepLength;
    private float mMajorStepLength;

    // attributs de couleur
    private int mDisabledColor;
    private int mDisabledColor2;
    private int mCenterColor;
    private int mTrackColor;
    private int mBorderColor;
    private int mValueColor;
    private int mStepColor;

    // attributs de pinceaux
    private Paint mCenterPaint;
    private Paint mTrackPaint;
    private Paint mBorderPaint;
    private Paint mTextPaint;
    private Paint mValuePaint;
    private Paint mMinorStepPaint;
    private Paint mMajorStepPaint;


    private float dpToPixel(float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    public Slider(Context context) {
        super(context);
        init(context, null);
    }

    public Slider(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        mBorderPaint = new Paint();
        mCenterPaint = new Paint();
        mTrackPaint = new Paint();
        mTextPaint = new Paint();
        mValuePaint = new Paint();
        mMajorStepPaint = new Paint();
        mMinorStepPaint = new Paint();



        mDisabledColor = ContextCompat.getColor(context, R.color.grey);
        mDisabledColor2 = ContextCompat.getColor(context, R.color.grey2);
        mCenterColor = ContextCompat.getColor(context, R.color.design_default_color_secondary);
        mBorderColor = ContextCompat.getColor(context, R.color.black);
        mTrackColor = ContextCompat.getColor(context, R.color.design_default_color_primary);
        mValueColor = ContextCompat.getColor(context, R.color.purple_200);

        mCenterDiameter = dpToPixel(DEFAULT_CENTER_DIAMETER);
        mTrackDiameter = dpToPixel(DEFAULT_TRACK_DIAMETER);
        mBorderDiameter = dpToPixel(DEFAULT_BORDER_DIAMETER);
        mMinorStepWidth = dpToPixel(DEFAULT_MINOR_STEP_WIDTH);
        mMajorStepWidth = dpToPixel(DEFAULT_MAJOR_STEP_WIDTH);
        mMinorStepLength = dpToPixel(DEFAULT_MINOR_STEP_LENGTH);
        mMajorStepLength = dpToPixel(DEFAULT_MAJOR_STEP_LENGTH);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextSize(40);
        mTextPaint.setTextAlign(Paint.Align.CENTER);

        //Récupération des attributs xml si présents
        if (attrs != null) {
            TypedArray attr = context.obtainStyledAttributes(attrs, R.styleable.Slider, 0, 0);
            mCenterDiameter = attr.getDimension(R.styleable.Slider_centerDiameter, mCenterDiameter);
            mTrackDiameter = attr.getDimension(R.styleable.Slider_trackDiameter, mTrackDiameter);
            mBorderDiameter = attr.getDimension(R.styleable.Slider_borderDiameter, mBorderDiameter);
            mBorderColor = attr.getColor(R.styleable.Slider_borderColor, mBorderColor);
            mCenterColor = attr.getColor(R.styleable.Slider_centerColor, mCenterColor);
            mTrackColor = attr.getColor(R.styleable.Slider_trackColor, mTrackColor);
            mStepColor = attr.getColor(R.styleable.Slider_stepColor, mStepColor);
            mEnabled = attr.getBoolean(R.styleable.Slider_enabled, mEnabled);
            attr.recycle();
        }

        if (mEnabled) {
            mCenterPaint.setColor(mCenterColor);
            mBorderPaint.setColor(mBorderColor);
            mTrackPaint.setColor(mTrackColor);
            mValuePaint.setColor(mValueColor);
        } else {
            mCenterPaint.setColor(mDisabledColor2);
            mBorderPaint.setColor(mBorderColor);
            mTrackPaint.setColor(mDisabledColor);
            mValuePaint.setColor(mDisabledColor);
        }

        mMinorStepPaint.setStrokeWidth(mMinorStepWidth);
        mMinorStepPaint.setStyle(Paint.Style.STROKE);
        mMinorStepPaint.setStrokeCap(Paint.Cap.ROUND);

        mMajorStepPaint.setStrokeWidth(mMajorStepWidth);
        mMajorStepPaint.setStyle(Paint.Style.STROKE);
        mMajorStepPaint.setStrokeCap(Paint.Cap.ROUND);

    }

    public void setEnabled (boolean enabled) {
        mEnabled = enabled;
        if (mEnabled) {
            mCenterPaint.setColor(mCenterColor);
            mBorderPaint.setColor(mBorderColor);
            mTrackPaint.setColor(mTrackColor);
            mValuePaint.setColor(mValueColor);
        } else {
            mCenterPaint.setColor(mDisabledColor2);
            mBorderPaint.setColor(mBorderColor);
            mTrackPaint.setColor(mDisabledColor);
            mValuePaint.setColor(mDisabledColor);
        }
        invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        RectF mOval = new RectF(mCenter.x - mTrackDiameter,
                mCenter.y - mTrackDiameter,
                mCenter.x + mTrackDiameter,
                mCenter.y + mTrackDiameter);

        canvas.drawCircle(mCenter.x, mCenter.y, mBorderDiameter, mBorderPaint);
        canvas.drawCircle(mCenter.x, mCenter.y, mTrackDiameter, mTrackPaint);

        canvas.drawArc(mOval, -90, mAngle, true, mValuePaint);
        canvas.drawCircle(mCenter.x, mCenter.y, mCenterDiameter, mCenterPaint);
        canvas.drawText(angleToValue(mAngle)+"%",
                getWidth() / 2,
                (getHeight() - mTextPaint.ascent()/2) / 2,
                mTextPaint);
        float rota=0;
        while (rota<Math.PI*2) {
            canvas.drawLine((float) (mCenter.x+(mCenterDiameter+(mTrackDiameter-mCenterDiameter-mMajorStepLength)/2)*Math.sin(rota)),
                    (float) (mCenter.y+(mCenterDiameter+(mTrackDiameter-mCenterDiameter-mMajorStepLength)/2)*Math.cos(rota)),
                    (float) (mCenter.x+(mCenterDiameter+(mTrackDiameter-mCenterDiameter+mMajorStepLength)/2)*Math.sin(rota)),
                    (float) (mCenter.y+(mCenterDiameter+(mTrackDiameter-mCenterDiameter+mMajorStepLength)/2)*Math.cos(rota)),
                    mMajorStepPaint);
            rota += (Math.PI/4);
        }
        rota = (float) (Math.PI/8);
        while (rota<Math.PI*2) {
            canvas.drawLine((float) (mCenter.x+(mCenterDiameter+(mTrackDiameter-mCenterDiameter-mMinorStepLength)/2)*Math.sin(rota)),
                    (float) (mCenter.y+(mCenterDiameter+(mTrackDiameter-mCenterDiameter-mMinorStepLength)/2)*Math.cos(rota)),
                    (float) (mCenter.x+(mCenterDiameter+(mTrackDiameter-mCenterDiameter+mMinorStepLength)/2)*Math.sin(rota)),
                    (float) (mCenter.y+(mCenterDiameter+(mTrackDiameter-mCenterDiameter+mMinorStepLength)/2)*Math.cos(rota)),
                    mMinorStepPaint);
            rota += (Math.PI/4);
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int suggestedHeight; // dimensions souhaitées par le slider
        int height; // dimensions calculées en tenant compte des spécifications du container

        suggestedHeight = (int) Math.max(dpToPixel(MIN_BORDER_DIAMETER), mBorderDiameter) * 2 + getPaddingTop() + getPaddingBottom();

        height = resolveSize(suggestedHeight, heightMeasureSpec);

        setMeasuredDimension(height, height);

        mCenter.x = height / 2;
        mCenter.y = height / 2;
    }


    private int angleToValue(int angle) {

        return (int) ((float)angle/360 * 100);
    }

    private int toAngle(Point point) {

        float uX = mPrePoint.x - mCenter.x;
        float vX = -(mPrePoint.y - mCenter.y);
        float uY = point.x - mCenter.x;
        float vY = -(point.y - mCenter.y);

        double angle = Math.atan2(vX, uX) - Math.atan2(vY, uY);
        angle = angle * 360 / (2 * Math.PI);
        if (!mStartRotation)
        {
            if (angle > 180) angle = 360 - angle;
            else if (angle < -180) angle = 360 + angle;
        }

        if (mStartRotation && angle <0) mAngleRotation = (int) (angle+360);
        else mAngleRotation += angle;

        System.out.println(angle);
        mStartRotation = false;
        mPrePoint = point;

        if (mAngleRotation <0) return 0;
        else if (mAngleRotation >360) return 360;
        return (int) mAngleRotation;
    }

    /******************************************************************************/
    /*                    Gestion des évènements                                  */

    /******************************************************************************/


    public interface SliderChangeListener {
        void onChange(float value);

        void onDoubleClick(float value);
    }

    public void setSliderChangeListener(SliderChangeListener sliderChangeListener) {
        mSliderChangeListener = sliderChangeListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mEnabled) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    if (!mDisableMove) {
                        mAngle = toAngle(new Point((int) event.getX(), (int) event.getY()));
                        if (mSliderChangeListener != null) mSliderChangeListener.onChange(mAngle);
                        invalidate();
                    }
                    break;
                case MotionEvent.ACTION_DOWN:
                    if (mDoubleClick) {
                        mDisableMove = true;
                        postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mDisableMove = false;
                            }
                        }, 200);
                        mAngle = 0;
                        if (mSliderChangeListener != null)
                            mSliderChangeListener.onDoubleClick(mAngle);
                        invalidate();
                    } else {
                        mDoubleClick = true;
                        postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mDoubleClick = false;
                            }
                        }, 500);
                    }
                    mPrePoint = new Point(mCenter.x, (int) (mCenter.y - mTrackDiameter));
                    mAngleRotation = 0;
                    mStartRotation = true;
                    invalidate();
                    break;
                default:
                    break;
            }
        }

        return true;
    }

    private class SavedState extends BaseSavedState {

        private int mSavedAngle;


        // utilisé par onSaveInstanceState
        public SavedState(Parcelable superState) {
            super(superState);
            mSavedAngle = mAngle;
        }

        // constructeur utilisé par le Creator pour recréer la classe SavedState
        private SavedState(Parcel in) {
            super(in);
            mSavedAngle = in.readInt();
        }

        public final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int i) {
                return new SavedState[0];
            }
        };

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeFloat(mSavedAngle);
        }
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        return new SavedState(super.onSaveInstanceState());
    }


    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        } else {
            super.onRestoreInstanceState(((SavedState) state).getSuperState());
            mAngle = ((SavedState) state).mSavedAngle;
        }
    }
}

