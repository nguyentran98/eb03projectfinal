package com.EB03.ProjectEB03;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FrameProcessor {
    private final static byte ESC = 0x06;
    private final static byte HEADER = 0x05;
    private final static byte TAIL = 0x04;

    private Data mData;
    private boolean frameCompleted = false;

    /**
     * Encode to data frame
     * @param c : array of data to send
     * @return Data frame
     */
    public byte[] toFrame(byte[] c) {
        List<Byte> frame = new ArrayList<Byte>();
        frame.add((byte) 0x05);
        frame.add((byte) (c.length / 256));
        frame.add((byte) (c.length % 256));
        int sum= ((c.length / 256) + (c.length % 256)) %256;
        for (byte x: c) {
            sum = (sum+x)%256;
            switch (x) {
                case HEADER:
                    frame.add((byte) (x + ESC));
                    break;
                case TAIL:
                    frame.add((byte) (x + ESC));
                    break;
                case ESC:
                    frame.add((byte) x);
                    frame.add((byte) 0x0C);
                    break;
                default:
                    frame.add((byte) (x));
                    break;
            }
        }
        frame.add(toTwoComplement(sum));
        frame.add((byte) 0x04);
        Log.i("FRAME", "toFrame: " +frame);
        return  toByteArray(frame);
    }

    /**
     * Receive data from data Frame
     * @param data
     * @return True if received a complete data frame
     *         False if not
     */
    public boolean fromFrame(byte data) {
        switch (data) {
            case HEADER:
                frameCompleted = false;
                mData = new Data();
                mData.mArray[mData.size] = data;
                return false;
            case TAIL:
                frameCompleted = true;
                mData.size ++;
                mData.mArray[mData.size] = data;
                return true;
            default:
                frameCompleted = false;
                mData.size ++;
                mData.mArray[mData.size] = data;
                return false;
        }
    }

    /**
     * Get data received
     * @return null if there are not a data frame complete received
     */
    public Data getData() {
        if (frameCompleted) return mData;
        return null;
    }

    public class Data {

        byte [] mArray;
        private int size;

        public Data () {
            mArray = new byte[1024];
            size=0;
        }

        /**
         * Get data payload
         * @return
         */
        public byte[] getFramePayload (){
            return Arrays.copyOfRange(mArray,3,size-1);
        }

        /**
         * Get length of data payload
         * @return
         */
        public int getFrameLength (){
            return (mArray[1]*256 + mArray[2]);
        }

        /**
         * Get CRC of data payload received
         * @return
         */
        public byte getExpectedFrameCRC (){
            int sum=0;
            for (int i=1; i<size-1; i++) {
                switch (mArray[i]) {
                    case HEADER+ESC:
                        sum = (sum + HEADER)%256;
                        break;
                    case TAIL+ESC:
                        sum = (sum + TAIL)%256;
                        break;
                    case ESC:
                        sum = (sum + ESC)%256;
                        i++;
                        break;
                    default:
                        sum = (sum + mArray[i])%256;
                        break;
                }
            }
            return toTwoComplement(sum);
        }

        /**
         * Get CRC received from frame
         * @return
         */
        public byte getFrameCRC (){
            return mArray[size-1];
        }

        /**
         * Check if data frame received is correct
         * @return
         */
        public boolean getFrameValidity (){
            if (getFrameCRC() == getExpectedFrameCRC())
                return true;
            return false;
        }
    }

    private byte toTwoComplement(int x) {
        x = ~x & 0xff;
        x += 1;
        return Short.valueOf(String.valueOf(x),10).byteValue();
    }

    private byte[] toByteArray(List<Byte> in) {
        final int n = in.size();
        byte ret[] = new byte[n];
        for (int i = 0; i < n; i++) {
            ret[i] = in.get(i);
        }
        return ret;
    }
}
