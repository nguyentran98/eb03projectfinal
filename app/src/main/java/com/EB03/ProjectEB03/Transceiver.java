package com.EB03.ProjectEB03;

public abstract class Transceiver {
    public static final int STATE_NOT_CONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    protected int mState;
    protected TransceiverListener mTransceiverListener;

    public Transceiver(){
        super();
        mState=0;
    }

    protected FrameProcessor mFrameProcessor;

    protected void attachFrameProcessor (FrameProcessor frameProcessor) {
        mFrameProcessor =frameProcessor;
    }

    protected void detachFrameProcessor () {
        mFrameProcessor = null;
    }

    public abstract void connect(String id);
    public abstract void disconnect();
    public abstract void send(byte[] b);

    public void setTransceiverListener(OscilloManager oscilloManager){
        mTransceiverListener = oscilloManager;
    };


    /**
     * Interface Listener
     */
    public interface TransceiverListener{
        void onTransceiverDataReceived();
        void onTransceiverStateChanged(int state);
        void onTransceiverConnectionLost();
        void onTransceiverUnableConnect();
    }

    /**
     * GETTER and SETTER
     */
    public void setState(int state){
        mState=state;
        if(mTransceiverListener!=null){
            mTransceiverListener.onTransceiverStateChanged(state);
        }
    }

    public int getState(){
        return mState;
    }

    public void connectLost(){
        if(mTransceiverListener!=null){
            mTransceiverListener.onTransceiverConnectionLost();
        }
    }
    public void connectFailed(){
        if(mTransceiverListener!=null){
            mTransceiverListener.onTransceiverUnableConnect();
        }
    }
}


