package com.EB03.ProjectEB03;

import static android.os.Build.VERSION.SDK_INT;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    static final int BT_CONNECT_CODE = 1;
    static final int PERMISSIONES_REQUEST_CODE = 0;
    static final int BT_ACTIVATION_REQUEST_CODE = 0;

    private Slider mSlider;
    private TextView mTv;
    private OscilloManager mOscilloManager;
    private BTManager mBTManager;

    final static String[]  BT_DANGEROUS_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTv = findViewById(R.id.tv);
        mSlider = findViewById(R.id.slider);

        mSlider.setSliderChangeListener(new Slider.SliderChangeListener() {
            @Override
            public void onChange(float value) {
                double mValue = value/360;
                mOscilloManager.setCalibrationDutyCycle(mValue);
            }

            @Override
            public void onDoubleClick(float value) {
                double mValue = value/360;
                mOscilloManager.setCalibrationDutyCycle(mValue);
            }
        });

        mOscilloManager = OscilloManager.getInstance();
        if (!mOscilloManager.hasTransceiver()) {
            mBTManager = new BTManager();
            mBTManager.attachFrameProcessor( new FrameProcessor());
            mOscilloManager.attachTransceiver(mBTManager);
        }

        setStatus (mOscilloManager.getStatus());

        mOscilloManager.setOscilloEventsListener(new OscilloManager.OscilloEventsListener() {
            @Override
            public void onOscilloStateChanged(int state) {
                setStatus (state);
            }

            @Override
            public void onOscilloUnableToConnect() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),"Connexion impossible",Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onOscilloConnectionLost() {

            }
        });

        verifyBtRights();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BT_CONNECT_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getStringExtra("device");
                    mOscilloManager.connect(address);
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int menuItem = item.getItemId();
        switch (menuItem) {
            case R.id.connect:
                Log.i("MENU","selection menu bluetooth");
                Intent BTconnect;
                BTconnect = new Intent(this, BTConnectActivity.class);
                startActivityForResult(BTconnect, BT_CONNECT_CODE);
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void verifyBtRights () {
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            Toast.makeText(this, "Application necessite un adapteur Bluetooth", Toast.LENGTH_SHORT).show();
            finish();
        }

        if (BluetoothAdapter.ACTION_REQUEST_ENABLE == null){
            Toast.makeText(this, "Adapteur pas presents", Toast.LENGTH_SHORT).show();
            finish();
        }

        if (SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(BT_DANGEROUS_PERMISSIONS, PERMISSIONES_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONES_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, "Les autorisations BT sont necessaires", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private void setStatus (final int state) {
        Log.i("BTStatus", "setStatus: "+state);
        final String stateString[] = new String[] {"Non connecte", "En connexion","Connecte"};
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTv.setText(stateString[state]);
                if (state == 2) mSlider.setEnabled(true);
                else mSlider.setEnabled(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        mOscilloManager.disconnect();
        super.onBackPressed();
    }
}