package com.EB03.ProjectEB03;

public class OscilloManager implements Transceiver.TransceiverListener{
    private final static byte SET_CALIBRATION_DUTY_CYCLE_REQUEST = 0x0A;

    private Transceiver mTransceiver = null;
    private OscilloEventsListener mOscilloEventsListener= null;

    private static OscilloManager outInstance;

    public static OscilloManager getInstance(){
        if (outInstance == null) {
            outInstance = new OscilloManager();
        }
        return outInstance;
    }

    public void attachTransceiver(Transceiver transceiver) {
        mTransceiver = transceiver;
        mTransceiver.setTransceiverListener(this);
    }

    public boolean hasTransceiver() {
        return mTransceiver != null;
    }

    public interface OscilloEventsListener  {
        void onOscilloStateChanged (final int state);
        void onOscilloUnableToConnect ();
        void onOscilloConnectionLost ();
    }

    void setOscilloEventsListener ( OscilloEventsListener oscilloEventsListener) {
        mOscilloEventsListener = oscilloEventsListener;
    }

    public int getStatus () {
        return mTransceiver.getState();
    }

    @Override
    public void onTransceiverDataReceived() {

    }

    @Override
    public void onTransceiverStateChanged(int state) {
        if (mOscilloEventsListener != null)
            mOscilloEventsListener.onOscilloStateChanged(state);
    }

    @Override
    public void onTransceiverConnectionLost() {
        if (mOscilloEventsListener != null)
            mOscilloEventsListener.onOscilloConnectionLost();
    }

    @Override
    public void onTransceiverUnableConnect() {
        if (mOscilloEventsListener != null)
            mOscilloEventsListener.onOscilloUnableToConnect();
    }

    public void connect (String address) {
        mTransceiver.connect(address);
    }

    public void disconnect () {
        mTransceiver.disconnect();
    }

    public void setCalibrationDutyCycle (double dc) {
        byte [] data ={SET_CALIBRATION_DUTY_CYCLE_REQUEST, (byte) (dc*100)};
        mTransceiver.send(data);
    }

}
