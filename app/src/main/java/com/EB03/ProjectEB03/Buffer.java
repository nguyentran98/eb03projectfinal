package com.EB03.ProjectEB03;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;

public class Buffer {
    private final byte[] buf;
    private int bStart;
    private int bEnd;
    private final int bSize;
    private boolean full;

    /**
     * Constructuer de Buffer
     * @param size : la capacite du Buffer
     */
    public Buffer(int size) {
        bSize = size;
        buf = new byte[size];
        bStart = bEnd = 0;
    }

    /**
     * Reprendre la taille de donnee stocke du buffer
     * @return
     */
    public synchronized int getDataSize() {
        if (full) return (bSize);
        int size = bEnd - bStart;
        if (size < 0) size += bSize;
        return size;
    }

    /**
     * Ecrire une tableau d'octets sur le Buffer
     * @param data
     * @throws BufferOverflowException
     */
    public synchronized void write(byte[] data) throws BufferOverflowException {
        for (byte b : data) {
            write (b);
        }
    }

    /**
     * Ecrire un octet sur le Buffer
     * @param b
     * @throws BufferOverflowException
     */
    public synchronized void write ( byte b) throws BufferOverflowException {
        if (!full) {
            buf[bEnd] = b;
            bEnd = (bEnd + 1) % bSize;
        } else {
            throw new BufferOverflowException();
        }
        if (bEnd == bStart) full = true;
        else full = false;
    }

    /**
     * Reprendre tout le donnee stocke dans le Buffer
     * @return
     */
    public synchronized byte[] getAll() {
        byte[] data = new byte[getDataSize()];
        int i = 0;
        while (getDataSize() > 0) {
            data[i] = get();
            i++;
        }
        return data;
    }

    /**
     * Reprendre un la premiere octet dans le Buffer
     * @return
     * @throws BufferUnderflowException
     */
    public synchronized byte get() throws BufferUnderflowException {
        if (getDataSize() > 0) {
            byte ret = buf[bStart];
            bStart = (bStart + 1) % bSize;
            full = false;
            return ret;
        } else throw new BufferUnderflowException();
    }
}
