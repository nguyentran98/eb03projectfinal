package com.EB03.ProjectEB03;

import static com.EB03.ProjectEB03.MainActivity.BT_ACTIVATION_REQUEST_CODE;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Set;

public class BTConnectActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private enum Action {START, STOP};
    private final static int RN42_CODE = 0x1F00;
    private ArrayAdapter<String> mPairedAdapter;
    private ArrayAdapter<String> mDiscoveredAdapter;
    private ListView mPairedList;
    private ListView mDiscoveredList;
    private BluetoothAdapter mBlueToothAdapter;
    private BroadcastReceiver mBroadcastReceiver;
    private Boolean mBroadcastRegistered;
    private Button mScann;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btconnect);

        mProgressBar = findViewById(R.id.progressBar);
        mPairedList = findViewById(R.id.list1);
        mDiscoveredList = findViewById(R.id.list2);
        mScann = findViewById(R.id.btScan);
        mScann.setOnClickListener(this);
//        mScann.setOnItemCLickListener

        mPairedAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        mDiscoveredAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        mPairedList.setAdapter(mPairedAdapter);
        mPairedList.setOnItemClickListener(this);
        mDiscoveredList.setAdapter(mDiscoveredAdapter);
        mDiscoveredList.setOnItemClickListener(this);

        mBlueToothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBlueToothAdapter.getBondedDevices();

        if (mBlueToothAdapter != null) {
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice pairDevice : pairedDevices) {
                    mPairedAdapter.add(pairDevice.getName() + "\n" + pairDevice.getAddress());
                }
            } else {
                mPairedAdapter.add("pas de peripheriques appaire");
            }
        }

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                        if (mDiscoveredAdapter.getCount() == 0) {
                            mDiscoveredAdapter.add("pas de peripherique trouve");
                        }
                        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
                        break;
                    case BluetoothDevice.ACTION_FOUND:
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        if ((device.getBondState() != BluetoothDevice.BOND_BONDED) && (device.getBluetoothClass().getDeviceClass() == RN42_CODE)) {
                            mDiscoveredAdapter.add (device.getName()+ "\n" + device.getAddress());
                        }
                        break;
                    default:
                        //null
                }
            }
        };
    }

    private void toggleBtScan() {
        if (mScann.getText().equals("Scanner")){
            btScan(Action.START);
            mProgressBar.setVisibility(View.VISIBLE);
            mScann.setText("Arreter");
            mDiscoveredAdapter.clear();

        } else {
            btScan(Action.STOP);
            mProgressBar.setVisibility(View.INVISIBLE);
            mScann.setText("Scanner");
        }
    }

    private void btScan (Action startstop) {
        if (startstop == Action.START) {
            IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            registerReceiver(mBroadcastReceiver,filter);
            filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver,filter);
            mBroadcastRegistered = true;
            mBlueToothAdapter.startDiscovery();
        } else {
            unregisterReceiver(mBroadcastReceiver);
            mBroadcastRegistered = false;
            mBlueToothAdapter.cancelDiscovery();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BT_ACTIVATION_REQUEST_CODE :
                if (requestCode == RESULT_OK) {
                    toggleBtScan();
                }
        }
    }

    @Override
    public void onClick (View view){
        switch (view.getId()) {
            case R.id.btScan:
                if (!mBlueToothAdapter.isEnabled()) {
                    Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBluetooth, BT_ACTIVATION_REQUEST_CODE);
                    return;
                }
                toggleBtScan();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent();
        mBlueToothAdapter.cancelDiscovery();
        String info = ((TextView) view).getText().toString();

        if (info.equals("aucun peripherique trouve") || info.equals("pas de peripherique appaire")) {
            setResult(RESULT_CANCELED);
            finish();;
            return;
        }

        if (info.length() > 17) {
            info = info.substring(info.length()-17);
            intent.putExtra("device", info);
            setResult(RESULT_OK,intent);
            Log.i("Item Click", "onItemClick: " + info);
            finish();
        }
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBlueToothAdapter != null)
            mBlueToothAdapter.cancelDiscovery();
    }
}