package com.EB03.ProjectEB03;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

public class BTManager extends Transceiver{

    // Unique UUID for this application (set the SPP UUID because expected incomming connection are of this type)
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // reference vers l'adaptateur
    private final BluetoothAdapter mAdapter;

    // réferences vers les Threads
    private BluetoothSocket mSocket = null;
    private ConnectThread mConnectThread = null;
    private ReadingThread mReadingThread = null;
    private WritingThread mWritingThread = null;

    // Constructeur par défaut
    public BTManager() {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
    }


    @Override
    public void connect(String id) {
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(id);
        disconnect();
        mConnectThread = new ConnectThread(device);
        setState (STATE_CONNECTING);
        mConnectThread.start();
    }

    @Override
    public void disconnect() {
        if (mSocket!= null){
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mSocket =null;
        mConnectThread =null;
        mReadingThread = null;
        mWritingThread = null;
    }

    @Override
    public void send(byte[] data) {
        if ((mState == BTManager.STATE_CONNECTED) &&
                (mWritingThread != null) && ( mFrameProcessor != null)) {
            mWritingThread.write (mFrameProcessor.toFrame (data));
        }
    }

    private class ConnectThread extends Thread {

        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp =null;
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
            }
            mSocket = tmp;
        }

        @Override
        public void run() {
            super.run();
            mAdapter.cancelDiscovery();

            try {
                mSocket.connect();

            } catch (IOException e) {
                disconnect();
                connectFailed();
            }

            mConnectThread = null;

            mReadingThread = new ReadingThread(mSocket);
            Log.i("ConnectThread", "Starting reading thread");
            mReadingThread.start();

            mWritingThread = new WritingThread(mSocket);
            Log.i("ConnectThread", "Starting writing thread");
            mWritingThread.start();
            setState(STATE_CONNECTED);
        }
    }

    /************************************************************************************
     /
     /                          THREADS de COMMUNICATION
     /
     /***********************************************************************************/

    private class WritingThread extends Thread {

        private OutputStream mOutStream = null;
        private Buffer mBuffer;
        public WritingThread(BluetoothSocket socket) {
            OutputStream tmpOutStream = null;
            try {
                tmpOutStream = socket.getOutputStream();
            } catch ( IOException e) {
                e.printStackTrace();
            }
            mOutStream = tmpOutStream;
            mBuffer = new Buffer(1024);
        }

        public void write(byte [] data) { mBuffer.write(data);}

        @Override
        public void run() {
            while (mSocket != null) {
                if (mBuffer.getDataSize() != 0) {
                    try {
                        mOutStream.write(mBuffer.getAll());
                    } catch ( IOException e ) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private class ReadingThread extends Thread {
        public ReadingThread(BluetoothSocket socket) {

        }
    }
}
